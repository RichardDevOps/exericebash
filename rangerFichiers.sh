#!/bin/bash

# # Le classeur de rangement

# ## Consigne

# Réaliser un script bash qui va classer dans des sous répertoire
# tous les fichiers d'un répertoire selon leurs extensions.
# Le script prend en argument le nom du répertoire cible
# (ou demande la saisie du dossier cible à trier).

# bonus : faire un fichier de log qui affiche :
#     - le nom du dossier trier
#     - la date à laquelle il a ete trier
#     - le resultat du tri (combien de fichier de chaque type)

# remarque : code doit etre documenté !
# function ListerExtensions(){

# }

#Fonction qui affiche le résumé des actions et qui nettoie les fichiers temporaires
#Non fini, le but est de parcourir le fichier et d'en extraire les données
# 560 xml
#  33 png
#  18 csv
#   5 sh
#   5 jpg
#   3 txt
#   3 md
# A la fin, je voulais supprimer les fichiers temporaires
function AfficherResume(){
    echo $fichier
}

#Fonctions qui déplace les fichiers dans les dossiers
function DeplacerDossiers(){
        while IFS= read -r line
        do
        # find $1 -name '*$line' -exec mv {} $1/dossier_$line \;
        find $1 -name '*$line' -exec mv {} $1/dossier_$line \;
            cd $1
            for fichiers in \*.$line;
            do
            mv $fichiers dossier_$line
            done
         done < listerextensions.txt
}

#Fonction qui crée les dossiers à partir du fichier créé dans la fonction ListerExtensions
function CreerDossiers(){
    while IFS= read -r line
    do
        echo "$line" | sed 's/[0-9]*//g' | sed 's/ //g' >> listerextensions.txt
    done < "$fichier"
    while IFS= read -r line
    do
        mkdir -p $1/dossier_$line
    done < listerextensions.txt
}


# On cherche nos fichiers avec la commande find, avec rev, on renverse les chaines de caractères pour avoir en premier champ l'extension
# On extrait ce champ avec cut, on rappelle la commande rev pour tout remettre à l'endroit, on passe tout en minuscule ave tr (au cas ou)
# On trie ave sort, on supprime les doublons avec uniq --count et on retrie de nouveau pour classer par quantités et on met le resultat dans un fichier avec la date du jour.
function ListerExtensions(){
    date=$(date +"%Y-%m-%d")
    fichier=listeFichiers_$date.txt
    # echo $date
    # ls -1 $1 > listeFichiers_$date.txt
    find $1 -type f | rev | cut -d. -f1 | rev  | tr '[:upper:]' '[:lower:]' | sort | uniq --count | sort -rn > $fichier
}


#Si l'utilisateur n'a pas donné de chemin ou un chemin éronné.
function DemanderChemin(){
    read -p "Veuillez donner le chemin du dossier à analyser : " chemin
    main $chemin
}


function main(){
    # echo $#
    if [[ $# -eq 1 ]] ; then
        # echo $#
        # On vérifie que l'utilsateur rentre bien un nombre.
        if  [ -d "$1" ] ; then
            ListerExtensions $1
        else
            echo "Ce chemin n'existe pas"
            DemanderChemin
        fi
    else
        DemanderChemin
    fi
    CreerDossiers $1
    DeplacerDossiers $1
    AfficherResume
}

# appel de ma fonction principale
main $@
